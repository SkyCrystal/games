package com.tutorial.main;

// imports

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.Random;

// Class extending our file GameObject
public class Player extends GameObject {

	// Define Random
	Random r = new Random();
	Handler handler;
	
	// Basic needs for Player
	public Player(int x, int y, ID id, Handler handler) {
		super(x, y, id);
		this.handler = handler;
		
	}
	
	// Collision 
	public Rectangle getBounds() {
		return new Rectangle(x, y, 32, 32);
	}
	
	// Happens every tick
	public void tick() {
		// Increase vel
		x += velX;
		y += velY;
		
		
		x = Game.clamp(x, 0, Game.WIDTH - 37);
		y = Game.clamp(y, 0, Game.HEIGHT - 60);
		
		handler.addObject(new Trail(x, y, ID.Trail, Color.white, 32, 32, 0.05, handler));
		
		collision();
		
	}
	
	private void collision() {
		for(int i = 0; i < handler.object.size(); i++) {
			GameObject tempObject = handler.object.get(i);
			
			if(tempObject.getID() == ID.BasicEnemy) {
				if(getBounds().intersects(tempObject.getBounds())) {
					//collision code
					HUD.HEALTH -= 2;
					
				}
			}
		}
			
	}
	
	// Rendering for the class
	public void render(Graphics g) {
		// Define g2d
		Graphics2D g2d = (Graphics2D) g;
		
		// Border
		g.setColor(Color.red);
		g2d.draw(getBounds());
		
		// Player color
		if(id == ID.Player) g.setColor(Color.white);
		//else if(id == ID.Player2) g.setColor(Color.blue);
		g.fillRect(x, y, 32, 32);
	}
	
	
}
